#!/usr/bin/env python3
import os,exiftool,datetime,numpy,argparse,re,glob

# List arguments
parser = argparse.ArgumentParser(prog='rename2.py',description='Rename photos and videos filenames.')
parser.add_argument('-c','--classify',action='store_true',help='Classify files by camera source.')
parser.add_argument('-m','--metadata',action='store_true',help='Display all keys in EXIF metadata.')
parser.add_argument('-f','--milli',action='store_true',help='Print milliseconds in timestamp.')
parser.add_argument('-p','--print',action='store_true',help='Print timestamp.')
parser.add_argument('-s','--shift',metavar="",type=int,help='Period to shift the timestamps (format H:M:S).')
args = parser.parse_args()

# List all files with defined extension
medias,imgs,vids = [],['JPEG','jpeg','JPG','jpg','HEIC','heic'],['MOV','mov','MP4','mp4','MPG','mpg']
for fname in os.listdir():
    if fname.split('.')[-1] in imgs+vids:
        medias.append(fname)

# List metadata
with exiftool.ExifTool() as et:
    metadata = et.get_metadata_batch(medias)

# Loop over files
for d in metadata:
    extension = d["SourceFile"].split('.')[-1]
    # Plot metadota if requested
    if args.metadata:
        print('\n',d["SourceFile"],'\n')
        for key in d.keys():
            print(key,':',d[key])
        continue
    # Classify medias by camera model if requested
    if args.classify:
        model = None
        for ext,key in [[imgs,'EXIF:Model'],[vids,'QuickTime:Model']]:
            if extension in ext and key in d.keys():
                model = '_'.join(str(d[key]).split())
                break
        assert model!=None, '%s: Camera model not found... Abort!'%d["SourceFile"]
        os.system('mkdir -p %s'%model)
        os.system('mv %s %s/'%(d["SourceFile"],model))
        continue
    # Extract timestamp from exiftool
    timestamp = None
    for ext,key in [[imgs,'Composite:SubSecCreateDate'],[vids,'QuickTime:CreationDate']]:
        if extension in ext and key in d.keys():
            timestamp = d[key]
            break
    #assert timestamp!=None, '%s: Datetime not found... Abort!'%d["SourceFile"]
    if timestamp==None:
        continue
    timestamp = numpy.array(re.split('[.: \-\+]',timestamp),dtype=int)
    if args.shift==None and args.milli==False:
        timestamp = '%04i%02i%02i_%02i%02i%02i.%s'%(*timestamp[:6],extension)
    elif args.shift==None and args.milli:
        timestamp = '%04i%02i%02i_%02i%02i%02i.%03i.%s'%(*timestamp[:7],extension)
    else:
        timestamp = datetime.datetime(*timestamp[:7]) + datetime.timedelta(hours=args.shift)
        timestamp = timestamp.strftime('%Y%m%d_%H%M%S.'+extension)
    if args.print:
        print(d["SourceFile"],'>',timestamp)
    elif os.path.exists(timestamp)==False:
        print(d["SourceFile"],'>',timestamp)
        os.system('mv %s %s'%(d["SourceFile"].replace(' ','\ '),timestamp))
    elif d["SourceFile"]==timestamp:
        continue
    else:
        idx = len(glob.glob(timestamp.replace('.','*.')))
        os.system('mv %s %s'%(d["SourceFile"].replace(' ','\ '),timestamp.replace('.','_%i.'%idx)))
        print(d["SourceFile"],'>',timestamp.replace('.','_%i.'%idx))

