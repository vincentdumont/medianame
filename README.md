# MediaName - Convert media filename into timestamp format

This small routine aims to automatically convert JPEG, JPG, HEIC, MOV, MP4 and MPG files into timestamp format. The program looks at the metadata of every file found in the given repository and search for a timestamp. It then renames each file using the year, month, day, hour, minute and second as follows: YYMMDD-HHMMSS.
